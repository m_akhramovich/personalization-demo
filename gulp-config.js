module.exports = function ()
{
    var instanceRoot = "C:\\inetpub\\wwwroot";
    var config = {
        websiteRoot: instanceRoot + "\\sc827\\Website",
        sitecoreLibraries: instanceRoot + "\\sc827\\Website\\bin",
        licensePath: instanceRoot + "\\Data\\license.xml",
        solutionName: "PersonalisationDemo",
        buildConfiguration: "Debug",
        buildPlatform: "Any CPU",
        buildToolsVersion: 15.0, //change to 15.0 for VS2017 support
        publishPlatform: "AnyCpu",
        runCleanBuilds: false,
        //scriptPath: [
        //    "src/Project/Meraas.Web/code/Assets/laguna/scripts/*.js",
        //    "!src/Project/Meraas.Web/code/Assets/laguna/scripts/*.min.js"
        //],
        //stylePath: [
        //    "src/Project/Meraas.Web/code/Assets/laguna/css/*.css",
        //    "!src/Project/Meraas.Web/code/Assets/laguna/css/*.min.css"
        //]
    };

    console.log(config.sitecoreLibraries, "Show Config");
    return config;
}