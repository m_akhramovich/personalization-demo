﻿using Sitecore.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Feature.GoalsAccomplition
{
    public class Templates
    {
        public struct CharityBanner
        {
            public struct Fields
            {
                public const string Text = "Text";
                public const string Link = "Link";
            }
        }

        public struct ContactUsBanner
        {
            public struct Fields
            {
                public const string Text = "Text";
                public const string Link = "Link";
            }
        }
    }
}