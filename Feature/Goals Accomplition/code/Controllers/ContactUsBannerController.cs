﻿using Sitecore.Feature.GoalsAccomplition.Models;
using Sitecore.Feature.GoalsAccomplition.Utility;
using System.Web.Mvc;

namespace Sitecore.Feature.GoalsAccomplition.Controllers
{
    public class ContactUsBannerController : Controller
    {
        public ActionResult Index()
        {
            var datasource = Helper.GetDataSourceItem();

            if(datasource == null)
            {
                return null;
            }

            return View(new ContactUsBannerViewModel(datasource));
        }
    }
}