﻿using Sitecore.Feature.GoalsAccomplition.Models;
using Sitecore.Feature.GoalsAccomplition.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sitecore.Feature.GoalsAccomplition.Controllers
{
    public class CharityBannerController : Controller
    {
        public ActionResult Index()
        {
            var datasource = Helper.GetDataSourceItem();

            if(datasource == null)
            {
                return null;
            }

            return View(new CharityBannerViewModel(datasource));
        }
    }
}