﻿using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Feature.GoalsAccomplition.Models
{
    public class ContactUsBannerViewModel
    {
        public ContactUsBannerViewModel(Item datasourceItem)
        {
            BannerText = new HtmlString(FieldRenderer.Render(datasourceItem, Templates.ContactUsBanner.Fields.Text));
            BannerLink = new HtmlString(FieldRenderer.Render(datasourceItem, Templates.ContactUsBanner.Fields.Link));
        }

        public IHtmlString BannerText { get; set; }
        public IHtmlString BannerLink { get; set; }
    }
}