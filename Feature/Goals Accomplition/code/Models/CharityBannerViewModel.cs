﻿using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;
using System.Web;

namespace Sitecore.Feature.GoalsAccomplition.Models
{
    public class CharityBannerViewModel
    {
        public CharityBannerViewModel(Item datasourceItem)
        {
            BannerText = new HtmlString(FieldRenderer.Render(datasourceItem, Templates.CharityBanner.Fields.Text));
            BannerLink = new HtmlString(FieldRenderer.Render(datasourceItem, Templates.CharityBanner.Fields.Link));
        }

        public IHtmlString BannerText { get; set; }
        public IHtmlString BannerLink { get; set; }
    }
}