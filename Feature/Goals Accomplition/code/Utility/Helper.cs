﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sitecore.Feature.GoalsAccomplition.Utility
{
    public class Helper
    {
        public static Item GetDataSourceItem() {
            var datasourceId = RenderingContext.Current.Rendering.DataSource;

            return ID.IsID(datasourceId) ? Sitecore.Context.Database.GetItem(datasourceId) : null;
        }
    }
}